FROM python:3.7
WORKDIR /code
COPY . .
RUN pip3 install -r requirements.txt
EXPOSE 5000
CMD ["python3.7", "app.py"]